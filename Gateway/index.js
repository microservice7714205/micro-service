require("dotenv").config();
var proxy = require("express-http-proxy");
var app = require("express")();
const http = require("http");
const jwt = require("jsonwebtoken");

const server = process.env.BASE_URL || "http://localhost:5000";

const authenticateUser = async (req, res, next) => {
  try {
    const token = req.get("accessToken");
    console.log("tooken", token);
    console.log("kjtw", jwt.verify(token, process.env.KEY));
    const { uuid = "" } = jwt.verify(token, process.env.KEY);
    console.log("uuid", uuid);
    if (!uuid) {
      res.status(400).json({ message: "Unauthorized" });
      return;
    }

    http.get(`${server}/customer/user/${uuid}`, (result) => {
      result.on("data", (data) => {
        console.log("DATAAA", data.toString());
        const response = JSON.parse(data.toString());

        if (response) {
          next();
        } else {
          res.status(400).json({ message: "Unauthorized" });
        }
      });
      return;
    });
  } catch (error) {
    res.status(400).json({ message: error });
  }
};

app.use("/customer", proxy(process.env.CUSTOMER));
app.use("/shopping", authenticateUser, proxy(process.env.SHOPPING));

app.listen(5000, () => {
  console.log("Gateway running on port 5000");
});
