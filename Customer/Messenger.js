const { Kafka, Partitioners } = require("kafkajs");
const { TOPICS } = require("./constant");

const kafka = new Kafka({
  clientId: "my-application",
  brokers: [process.env.KAFKA_SERVER],
});

const producer = kafka.producer({
  createPartitioner: Partitioners.LegacyPartitioner,
});

const consumer = kafka.consumer({ groupId: "my-customer-group" });

const init = async () => {
  await producer.connect();
  await consumer.connect();
};

init().then(() => {
  producer.send({
    topic: TOPICS.INITIAL_TOPIC,
    messages: [
      {
        value: JSON.stringify({ initialSuccess: "true" }),
      },
    ],
  });
  consumer.subscribe({ topic: TOPICS.INITIAL_TOPIC, fromBeginning: true });
  consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      const value = JSON.parse(message.value.toString());
      console.log({ value });
    },
  });
});

const sendMessage = async (data, topic, key) => {
  producer.send({
    topic: topic,
    messages: [{ value: JSON.stringify(data), key }],
  });
};

const cleanup = async () => {
  producer.disconnect();
  consumer.disconnect();
};
init().then(() => {});

module.exports = { cleanup, sendMessage };
