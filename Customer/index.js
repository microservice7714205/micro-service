require("dotenv").config();
const express = require("express");
const { cleanup, sendMessage } = require("./Messenger");
const {
  getUserById,
  createUser,
  addItemToCart,
} = require("./User/user.service");
require("./Messenger");
const { v4: uuid } = require("uuid");
const { default: mongoose } = require("mongoose");
const jwt = require("jsonwebtoken");
const { faker } = require("@faker-js/faker");
const userModel = require("./User/user.model");

const createDummyUsers = async () => {
  let users = [];
  for (let i = 0; i < 10; i++) {
    users.push({
      uuid: faker.datatype.uuid(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      cart: [],
    });
  }

  userModel.create(users);
};

const createDBConnection = async () => {
  console.log("\n\n\n\n Connected to DB");
  mongoose.set("strictQuery", false);
  mongoose.connect(process.env.DB_CONNECTION);
  mongoose.connection.on("open", async () => {
    const userCount = await userModel.count();
    if (!userCount) {
      createDummyUsers();
    }

    const app = express();

    app.use(express.json());
    const port = 8080 || process.env.PORT;

    app.get("/user/:uuid", async (req, res) => {
      let user = await getUserById(req.params.uuid);
      var token = jwt.sign({ ...user }, process.env.KEY);
      res.status(200).json({ user, token });
      return;
    });

    app.post("/user", async (req, res) => {
      let user = await createUser({ ...req.body, uuid: uuid() });
      var token = jwt.sign({ ...user }, process.env.KEY);
      res.status(200).json({ user, token });
      return;
    });

    app.put("/add/item", async (req, res) => {
      let { uuid = "" } = jwt.verify(req.get("accessToken"), process.env.KEY),
        itemInfo = req.body;

      if (uuid) {
        await addItemToCart(uuid, itemInfo);
        console.log("{ uuid: itemInfo.uuid }", { uuid: itemInfo.uuid });
        sendMessage({ uuid: itemInfo.uuid }, "item-topic", "ADD_ITEM");
        res.status(200).json({ message: "Success" });
      } else {
        res.status(400).json({ message: "Unauthorized" });
      }
      return;
    });

    app.listen(port, () => {
      console.log(`Listening to port ${port}`);
    });
  });
};

createDBConnection();
process.on("exit", () => {
  console.log("EXITED!!");
});

//catches ctrl+c event
process.on("SIGINT", async () => {
  await cleanup();
  process.exit();
});
