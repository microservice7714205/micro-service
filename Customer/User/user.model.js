const { Schema, model, default: mongoose } = require("mongoose");

const userSchema = new Schema({
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },

  cart: [
    {
      itemId: {
        type: String,
      },

      name: {
        type: String,
      },

      price: {
        type: String,
      },
    },
  ],

  uuid: {
    type: String,
  },
});

const userModel = model("users", userSchema);
module.exports = userModel;
