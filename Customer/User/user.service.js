const userModel = require("./user.model");

const createUser = async (data) => {
  let user = await new userModel(data).save();
  return user.toObject();
};

const getUserById = async (uuid) => {
  let user = await userModel.find({ uuid }).lean();
  return user[0];
};

const addItemToCart = async (userId, itemInfo) => {
  let user = await userModel.findOneAndUpdate(
    { uuid: userId },
    { $addToSet: { cart: itemInfo } }
  );
  return user;
};
module.exports = { createUser, getUserById, addItemToCart };
