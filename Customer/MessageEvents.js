const { TOPICS } = require("./constant");
const { consumeMessage } = require("./Messenger");

const consumeAddToCart = async () => {
  consumeMessage(TOPICS.ADD_TO_CART, (message, topic, partition) => {
    console.log("ADD TO CART: Message", JSON.parse(message));
  });
};

consumeAddToCart();
