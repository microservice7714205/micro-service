const { Kafka, Partitioners } = require("kafkajs");
const { TOPIC } = require("./constant");

const kafka = new Kafka({
  clientId: "my-application",
  brokers: ["localhost:9092"],
});

const producer = kafka.producer({
  createPartitioner: Partitioners.LegacyPartitioner,
});

const consumer = kafka.consumer({ groupId: "authentication-group" });

const init = async () => {
  await producer.connect();
  await consumer.connect();
  await consumer.subscribe({
    topic: TOPIC.AUTH_TOPIC,
    fromBeginning: true,
  });

  consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      let data = JSON.parse(message.value.toString());
      console.log("Aut data:", data);
      producer.send({
        topic: TOPIC.AUTH_COMPLETE_TOPIC,
        messages: [{ value: JSON.stringify({ success: true }) }],
      });
    },
  });
  // await Promise.all([producer.connect(), consumer.connect()]).then(
  //   async ([producer, consumer]) => {
  //     console.log(";kjhvgds vdsdsds", consumer)
  //     await consumer.subscribe({
  //       topic: TOPIC.AUTH_TOPIC,
  //       fromBeginning: true,
  //     });

  //     consumer.run({
  //       eachMessage: async ({ topic, partition, message }) => {
  //         console.log("AUTH MESSAGE", message);
  //       },
  //     });
  //   }
  // );
};

init();
