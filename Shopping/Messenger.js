const { Kafka, Partitioners } = require("kafkajs");
const { TOPICS } = require("./constant");
const itemModel = require("./Item/item.model");

const kafka = new Kafka({
  clientId: "my-application",
  brokers: [process.env.KAFKA_SERVER],
});

const producer = kafka.producer({
  createPartitioner: Partitioners.LegacyPartitioner,
});

const consumer = kafka.consumer({ groupId: "my-shopping-group" });

const init = async () => {
  await producer.connect();
  await consumer.connect();

  return true;
};

init().then(async () => {
  console.log("Connection established with kafka!!");
  consumer.subscribe({
    topic: "item-topic",
    fromBeginning: true,
  });

  consumer.run({
    eachMessage: async ({ message, topic, partition }) => {
      const msg = JSON.parse(message.value.toString());
      const key = message.key.toString();
      switch (key) {
        case "ADD_ITEM": {
          let { uuid } = msg;
          const item = await itemModel.findOne({ uuid });
          item.potentialBuyers = item.potentialBuyers + 1;
          await item.save();
          // itemModel.findOneAndUpdate(
          //   { uuid },
          //   { $inc: { potentialBuyers: 1 } }
          // );
          break;
        }
      }
    },
  });
});

const cleanup = () => {
  console.log("===== Disconnecting consumers =====");
  producer.disconnect();
  consumer.disconnect();
};

module.exports = {
  cleanup,
};
