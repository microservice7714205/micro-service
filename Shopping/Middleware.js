const { authenticateUserMessage } = require("./Messenger");

const authenticateUser = (req, res, next) => {
  authenticateUserMessage(
    req.get("accessToken"),
    (message, topic, partition) => {
      console.log("Authentication Message: ", message);
      next();
    }
  );
};

module.exports = { authenticateUser };
