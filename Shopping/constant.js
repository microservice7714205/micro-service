const TOPICS = {
  INITIAL_TOPIC: "inital-topic",
  AUTH_TOPIC: "auth_topic",
  AUTH_COMPLETE_TOPIC: "auth_complete_topic",
};

module.exports = { TOPICS };
