const { Schema, model } = require("mongoose");

const itemSchema = new Schema({
  name: {
    type: String,
  },

  price: {
    type: String,
  },

  uuid: {
    type: String,
  },

  potentialBuyers: {
    type: Number,
    default: 0,
  },
});

const itemModel = model("items", itemSchema);

module.exports = itemModel;
