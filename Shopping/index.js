// require("dotenv").config();
// const express = require("express");
// const { TOPICS } = require("./constant");
// // const { cleanup } = require("./Messenger");
// const app = express();

// app.use(express.json());
// const port = 8081 || process.env.PORT;
// const { Client } = require("pg");

// const connectToDB = async () => {
//   // const pool = new Pool({
//   //   connectionString: `postgres://${process.env.USERNAME}:${process.env.PWD}@${process.env.HOST}/${process.env.DB_NAME}`,
//   //   // user: process.env.USERNAME,
//   //   // host: process.env.HOST,
//   //   // database: process.env.DB_NAME,
//   //   // password: process.env.PWD,
//   //   // port: process.env.PORT,
//   // });

//   // await pool.connect();

//   // const client = new Client({
//   //   connectionString: `postgres://${process.env.USERNAME}:${process.env.PWD}@${process.env.HOST}/${process.env.DB_NAME}`,
//   //   // user: process.env.PGUSER,
//   //   // host: process.env.PGHOST,
//   //   // database: process.env.PGDATABASE,
//   //   // password: process.env.PGPASSWORD,
//   //   // port: process.env.PGPORT,
//   // });
//   const client = new Client({
//     connectionString: `postgres://${process.env.USERNAME}:${process.env.PASSWORD}@${process.env.HOST}/${process.env.DB_NAME}`,
//   });
//   await client.connect();
//   return client;
// };

// connectToDB().then(async (client) => {
//   console.log("\n\n\n Connected to postgres!!!!");
//   const createTableQuery = `
//     CREATE TABLE IF NOT EXISTS items (
//       id SERIAL PRIMARY KEY,
//       name VARCHAR(255) NOT NULL,
//       price VARCHAR(255) NOT NULL,
//       potential_buyer INTEGER DEFAULT 0
//     )
//   `;

//   client.query(createTableQuery, (err, res) => {
//     if (err) {
//       console.error("Error creating table", err);
//     } else {
//       console.log("Table created successfully");
//     }
//   });

//   app.get("/item/add", async (req, res) => {
//     res.json({ Success: true });
//   });
// });

// const server = app.listen(port, () => {
//   console.log(`Listening to port ${port}`);
// });

require("dotenv").config();
const express = require("express");
const { TOPICS } = require("./constant");
const { cleanup } = require("./Messenger");
const mongoose = require("mongoose");
const itemModel = require("./Item/item.model");
const { v4: uuid } = require("uuid");

const { faker } = require("@faker-js/faker");

mongoose.set("strictQuery", false);

const app = express();

app.use(express.json());

let connection;

const connectToDB = async () => {
  mongoose.connect(process.env.DB_CONNECTION);
  connection = mongoose.connection;
  return connection;
};

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const createDummyItems = async () => {
  let items = [];
  for (let i = 0; i < 10; i++) {
    items.push({
      uuid: faker.datatype.uuid(),
      name: faker.internet.userName(),
      price: getRandomInt(10, 70),
      potentialBuyers: 0,
    });
  }

  itemModel.create(items);
};

connectToDB().then((connection) => {
  connection.on("open", async () => {
    console.log("Connected to DB");

    // Creating multiple dummy items
    let totalCount = await itemModel.count();
    if (!totalCount) {
      createDummyItems();
    }

    app.use(express.json());
    const port = 8081 || process.env.PORT;

    app.listen(port, () => {
      console.log(`Listening to port ${port}`);
    });

    app.post("/item/add", async (req, res) => {
      let item = await new itemModel({ ...req.body, uuid: uuid() }).save();
      res.status(200).json({ item });
      return;
    });
  });
});
process.on("SIGINT", () => {
  connection.close();
  process.exit(0);
});

process.on("exit", () => {
  cleanup();
  connection.close();
});
